import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  /*OnChanges,
  SimpleChanges,*/
  DoCheck,
  AfterContentInit, OnDestroy
} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, DoCheck, AfterContentInit, OnDestroy {

  @Input() users: [{ nome: string, citta: string }];
  @Output() index = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  removeUser(index) {
    this.index.emit(index);
  }

  /*ngOnChanges(cambio: SimpleChanges) {
    console.log('sono ngOnChanges di ' + cambio);
  }
*/
  ngDoCheck() {
    console.log('sono ngDoCheck');
  }

  ngAfterContentInit(): void {
    console.log('sono ngAfterContentInit');
  }

  ngOnDestroy(): void {
    console.log('il component è stato distrutto');
  }
}
