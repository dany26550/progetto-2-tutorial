import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor() {
    console.log('sono il costruttore');
    setTimeout(() => {
      this.nome = 'sono un h1';
    }, 2000);
  }

  utenti = [];
  value = true;

  title = 'project';
  nome: string;
  stampa: string;
  testo2: string;

  ngOnInit() {
    console.log('sono ngOnInit');
    this.utenti = [{nome: 'Max', citta: 'Roma'},
      {nome: 'Mario', citta: 'Napoli'},
      {nome: 'Simona', citta: 'Milano'}];
  }

  getClick(event) {
    console.log(event);
  }

  getText(event) {
    console.log(event.target.value);
    this.stampa = event.target.value;
  }

  getData(input) {
    console.log(input);
  }

  getUser(event: { nome: string, citta: string }) {
    this.utenti.push({nome: event.nome, citta: event.citta});
  }

  getRemoveUser(event) {
    this.utenti.splice(event, 1);
  }
}
